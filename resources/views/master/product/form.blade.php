@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Product</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <!-- Row Form -->
                    <div class="row">
                        <div class="col">
                            <form id="form-action" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-product-photo">Photo</label>
                                            <input type="file" class="form-control" name="product-photo" id="fl-product-photo">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-name">Name</label>
                                            <input type="text" class="form-control" name="name" id="txt-name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-quantity">Quantity</label>
                                            <input type="number" class="form-control" name="quantity" id="txt-quantity" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col">
                                        <div class="form-group d-flex">
                                            <button class="btn btn-primary ml-auto" type="submit">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
    <script>
        var exists = '{{ $exists }}';

        var renderComponents = () => {
            if(exists == 'create') {
                $('#form-action').attr('action', '{{ route("master.product.store") }}')
            }else{
                $('#txt-name').val('{{ $exists != "create" ? $exists->name : "" }}')
                $('#txt-quantity').val('{{ $exists != "create" ? $exists->quantity : "" }}')

                $('#form-action').prepend(`<input type="hidden" name="_method" value="PUT">`)
                $('#form-action').attr('action', '{{ url("master/product") }}/'+'{{ $exists != "create" ? $exists->id : "create" }}')
            }
        }

        $(document).ready(function() {
            renderComponents();
        })
    </script>
@endpush
