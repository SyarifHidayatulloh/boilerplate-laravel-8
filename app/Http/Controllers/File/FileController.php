<?php

namespace App\Http\Controllers\File;

use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

// Controller
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public static function save_image($file, $type)
    {
        $filename = rand(1, 100).time().rand(1, 100).'.'.$file->getClientOriginalExtension();
        $path = 'assets/images/'.$type;

        if (!file_exists($path)) {
            mkdir($path, 666, true);
        }

        $path = $path.'/'.$filename;

        Image::make($file)->save($path);
        return $path;
    }
}
