<?php

namespace App\Http\Controllers\Master;

use Auth;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

// Controller
use App\Http\Controllers\Controller;
use App\Http\Controllers\File\FileController;

// Models
use App\Models\Master\MasterProduct;

class MasterProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('master.product.index');
    }

    public function create()
    {
        $data['exists'] = 'create';

        return view('master.product.form', $data);
    }

    public function store(Request $request)
    {
        try {
            if($request->file('product-photo') != null){
                $photo = FileController::save_image($request->file('product-photo'), 'product');
                $request->request->add(['photo' => $photo]);
            }

            $data = MasterProduct::create($request->all());

            return redirect(route('master.product.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil ditambahkan'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => $e->getMessage(),
                // 'text' => 'Kesalahan pada sistem',
            ]);
        }
    }

    public function edit($product)
    {
        $product = MasterProduct::find($product);

        $data['exists'] = $product;

        return view('master.product.form', $data);
    }

    public function update(Request $request, $product)
    {
        try {
            if($request->file('product-photo') != null){
                $photo = FileController::save_image($request->file('product-photo'), 'product');
                $request->request->add(['photo' => $photo]);
            }
            
            $data = MasterProduct::find($product);
            $data->fill($request->all())->save();

            return redirect(route('master.product.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil diubah'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => 'Kesalahan pada sistem',
            ]);
        }
    }

    public function destroy($product)
    {
        try {
            $check = MasterProduct::find($product);

            if(!$check) {
                return redirect()->back()->with('alert', [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'Gagal ! ',
                    'text' => 'Data tidak ditemukan atau sudah dihapus'
                ]);
            }

            $check->delete();

            return redirect(route('master.product.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil dihapus'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => 'Kesalahan pada sistem',
            ]);
        }
    }

    public function table(Request $request) {
        $data = MasterProduct::select();

        return Datatables::of($data)
            ->addColumn('action', function($data) {
                return '<center>
                    <a href="'.route("master.product.edit", ['product' => $data->id]).'" class="btn btn-sm btn-warning text-white m-0">Edit</a>
                    <button id="'.route("master.product.destroy", ['product' => $data->id]).'" class="btn btn-sm btn-danger text-white m-0 btn-hapus">Hapus</a>
                </center>';
            })
            ->make(true);
    }
}
