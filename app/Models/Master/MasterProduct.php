<?php

namespace App\Models\Master;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class MasterProduct extends Model
{
    use Uuid;

    public $incrementing = false;
    protected $keyType = 'uuid';
    protected $table = 'master_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'photo',
        'quantity',
    ];
}
