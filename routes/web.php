<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

    Route::group(['prefix' => 'master', 'as' => 'master.'], function () {
        // -- Master Product
        Route::resource('/product', App\Http\Controllers\Master\MasterProductController::class)
            ->only(['index', 'create', 'store', 'edit', 'update']);
        Route::get('/product/{product}/destroy', [App\Http\Controllers\Master\MasterProductController::class, 'destroy'])->name('product.destroy');
    });
});
